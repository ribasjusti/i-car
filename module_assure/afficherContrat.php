<?php
    session_start();
    //vérification que l'utilisateur soit connecté pour accéder à la page, et qu'il soit un assuré
    if (!isset($_SESSION['identifiant'])|| ($_SESSION['profil']!="assure")){
        header('Location: connexion.php'); //à changer peut-être
        exit();
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
    <title>I-Car</title>
    <meta name="Amandine" lang="fr" content="menu assuré"/>
    <meta charset="UTF8"/>
    <link rel="icon" type="image/png" href="../img/icon.png">
    <link rel="stylesheet" type="text/css" href="../css/designGlobal.css" />
    <link rel="stylesheet" type="text/css" href="../css/form.css" />
    </head>
    <body>


      <?php

        /*Fonction pour récupérer tous les info du contrat*/
        function recupInfoContrat(){
          $row = 0;
    			$contrat = array(); //tableau qui contient les info du contrat
          $tabKeys = array(); //tableau qui contient toutes les clés du tableau $contrat
    			if (($handle = fopen("../csv/contrats.csv", "r"))) {
    	    	while (($data = fgetcsv($handle, 1000, ";"))) {
    					if($row == 0){
    						// si on est à la première ligne du csv, on récupère les clés
    						$tabKeys = $data;
    					} else {
    						if($_POST["numero"] == $data[0]){
                  // si on est à la ligne du bon contrat, on stocke les informations
                  $i = 0;
      						foreach ($tabKeys as $key) {
      							$contrat[$key] = $data[$i];
      							$i ++;
        					}
              	}
    					}
    					$row++;
    	    	}
    				fclose($handle);
    	    }
    			return($contrat);
        }


        /*Fonction pour récupérer tous les info de la carte verte*/
        function recupInfoCarteVerte($numeroCarteVerte){
          $row = 0;
    			$carteVerte = array(); //tableau qui contient les info de la carte verte
          $tabKeys = array(); //tableau qui contient toutes les clés du tableau $carteVerte
    			if (($handle = fopen("../csv/cartesVertes.csv", "r"))) {
    	    	while (($data = fgetcsv($handle, 1000, ";"))) {
    					if($row == 0){
    						// si on est à la première ligne du csv, on récupère les clés
    						$tabKeys = $data;
    					} else {
    						if($numeroCarteVerte == $data[0]){
                  // si on est à la ligne de la bonne carte verte, on stocke les informations
                  $i = 0;
      						foreach ($tabKeys as $key) {
      							$carteVerte[$key] = $data[$i];
      							$i ++;
        					}
              	}
    					}
    					$row++;
    	    	}
    				fclose($handle);
    	    }
    			return($carteVerte);
        }


        /*Fonction pour récupérer tous les info de l'assurance*/
        function recupInfoAssurance($codeAssureur){
          $row = 0;
    			$assurance = array(); //tableau qui contient les info de l'assurance
          $tabKeys = array(); //tableau qui contient toutes les clés du tableau $assurance
    			if (($handle = fopen("../csv/assureurs.csv", "r"))) {
    	    	while (($data = fgetcsv($handle, 1000, ";"))) {
    					if($row == 0){
    						// si on est à la première ligne du csv, on récupère les clés
    						$tabKeys = $data;
    					} else {
    						if($codeAssureur == $data[0]){
                  // si on est à la ligne de la bonne carte verte, on stocke les informations
                  $i = 0;
      						foreach ($tabKeys as $key) {
      							$assurance[$key] = $data[$i];
      							$i ++;
        					}
              	}
    					}
    					$row++;
    	    	}
    				fclose($handle);
    	    }
    			return($assurance);
        }


        /* Fonction pour afficher un contrat */
        function afficherContrat($contrat, $carteVerte, $assurance){
          $validiteContrat = RenouvellementDate($contrat["date"]);
          if($validiteContrat == "invalide"){
            $image = "invalide.png";
          } else {
            $image = "valider.png";
          }
          $validite = array("A", "B", "BG", "CY", "CZ", "D", "DK", "E", "EST", "F", "FN", "GB", "GR", "H", "I", "IRL", "IS", "L", "LT", "LV", "M", "N", "NL", "P", "PL", "RO", "S", "SK", "SLO", "CH", "AL", "AND", "BIH", "BY", "HR", "IL", "IR", "MA", "MD", "MK", "RUS", "SRB", "TN", "TR", "UA");
          $dateDeb = explode("-", $carteVerte["dateDebut"]);
       		$annee = $dateDeb[0] + 1;
       		$dateFin = $annee."-".$dateDeb[1]."-".$dateDeb[2];
          echo("<div class='contrat'>
            <p>Validité du contrat : ".$validiteContrat."</p>
            <img src='../img/".$image."' style='height:50px'/>
            <h4>Informations de la carte verte : </h4>
            <p>Numéro de carte verte : ".$carteVerte["numero"]."</p>
            <p>Valable du : ".$carteVerte["dateDebut"]." , au : ".$dateFin."</p>
            <p>Code pays / code assureur - numéro : ".$carteVerte["codePays"]." ".$carteVerte["codeAssureur"]."</p>
            <p>Numéro d'immatriculation : ".$carteVerte["plaque"]."</p>
            <p>Catégorie du véhicule : ".$carteVerte["catégorie"]."</p>
            <p>Marque du véhicule : ".$carteVerte["marque"]."</p>
            <p>Validité territoriale : </p>
            <table>");
          $vInterdites = explode("-", $carteVerte["validiteTerritoriale"]);
          echo("<tr>");
          $i = 1;
          foreach ($validite as $value) {
            if(in_array($value, $vInterdites)){
              echo("<td style='background-color: #5a5958'>".$value."</td>");
            } else {
              echo("<td>".$value."</td>");
            }
            if($i % 14 == 0){
              echo("</tr><tr>");
            }
            $i ++;
          }

          echo("</tr></table>
            <p>Nom du souscripteur : ".$_SESSION["nom"]." ".$_SESSION["prenom"]."</p>
            <p>Adresse du souscripteur : ".$_SESSION["adresse"]." ".$_SESSION["ville"]." ".$_SESSION["codePostal"]."</p>
            <p>Carte délivrée par : ".$assurance["nom"]." | ".$assurance["adresse"]." ".$assurance["ville"]." ".$assurance["codePostal"]."</p>");

          //Carte grise
          if ($contrat["carteGrise"] == ''){
            echo "<h4>Carte Grise :</h4>";
            echo "<p>Vous n'avez pas ajouté de carte grise. Pour ce faire, il faut uploader une image de votre carte grise.</p>";
            echo"<form action='uploadCarteGrise.php?numero=".$_POST["numero"]."' method='post' enctype='multipart/form-data' class='form'>
              Selectionner l'image (format png, jpeg ou jpg) à uploader :
              <input type='file' name='fileToUpload' id='fileToUpload' class='btn'>
              <input type='submit' value='Uploader le fichier' name='submit' class='btn'>
              </form>";

          }else{
            echo "<h4>Carte Grise :</h4>";
            echo "<div id='catreGrise' style='text-align:center;'>";
            echo '<img src="../CartesGrises/'.$contrat["carteGrise"].'" alt="Carte grise" style="width:90%;">';
            echo "</div>";
          }

          //Permis
          if ($contrat["permis"] == ''){
            echo "<h4>Permis de conduire :</h4>";
            echo "<p>Vous n'avez pas ajouté de Permis de conduire. Pour ce faire, il faut uploader une image de votre permis. Attention mettez bien le recto et le verso sur le même fichier.</p>";
            echo"<form action='uploadPermis.php?numero=".$_POST["numero"]."' method='post' enctype='multipart/form-data' class='form'>
              Selectionner l'image (format png, jpeg ou jpg) à uploader :
              <input type='file' name='fileToUpload' id='fileToUpload' class='btn'>
              <input type='submit' value='Uploader le fichier' name='submit' class='btn'>
              </form>";

          }else{
            echo "<h4>Permis :</h4>";
            echo "<div id='permis' style='text-align:center;'>";
            echo '<img src="../Permis/'.$contrat["permis"].'" alt="Permis" style="width:90%;">';
            echo "</div>";
          }


          echo("<h4>QR code associé à ce contrat : </h4>
       			<a href='../QRcode/".$_POST["numero"].".png' download='".$_POST["numero"].".png'><img src='../QRcode/".$_POST["numero"].".png'/></a>
       			<p><i>(Cliquez sur le QR code pour le télécharger)</i></p>
            </div>");

        }

        function RenouvellementDate($dateContrat){
          $annee = date("Y");
    			$mois = date("m");
    			$jour = date("d");
    			$dateM = explode('-', $dateContrat);
    			$validite = "valide";
     			if ($dateM[0] < $annee){
    				$validite = "invalide";
    				if ($annee - $dateM[0] == 1 && $dateM[1] >= $mois){
    					$validite = "valide";
    					if ($dateM[2] >= $jour){
    						$validite = "valide";
    					} else {
    						$validite = "invalide";
    					}
    				}
    			}
     			return($validite);
     		}


        $contrat = recupInfoContrat();
        $carteVerte = recupInfoCarteVerte($contrat["numCarteVerte"]);
        $assurance = recupInfoAssurance($contrat["codeAssureur"]);
        afficherContrat($contrat, $carteVerte, $assurance);

      ?>



    </body>
</html>
