<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>I-Car</title>
  <link rel="icon" type="image/png" href="../img/icon.png">
  <link rel="stylesheet" type="text/css" href="../css/designGlobal.css" />
  <link rel="stylesheet" type="text/css" href="../css/navbar.css" />
</head>
<body>

  <div class="nav">
    <input type="checkbox" id="nav-check">
    <div class="nav-header">
      <div class="nav-title">
        <a href="./menu_assure.php"><img style="width: 50px" src="../img/icon.png"/></a>
      </div>
    </div>
    <div class="nav-btn">
      <label for="nav-check">
        <span></span>
        <span></span>
        <span></span>
      </label>
    </div>

    <div class="nav-links">
      <a href="./pageProfil.php">Profil</a>
      <a href="./cAmiable.php">Constats</a>
      <a href="./pageAccueilSinistres.php">Sinistres</a>
      <a href="./contacterAssurance.php">Messagerie</a>
      <a href="./dVenteVehicule.php">Cession vehicule</a>
      <a href="../deconnexion.php?connexion=out">Déconnexion</a>
    </div>
  </div>

    <h1 class="titre">Permis</h1>

    <div class="affichage">

    <?php
        $ligneSuppr=0;  // numéro de la ligne
        $row = 0;
        $contrat = array(); //tableau qui contient les info du contrat
        $tabKeys = array(); //tableau qui contient toutes les clés du tableau $contrat
        if (($handle = fopen("../csv/contrats.csv", "r"))) {
        while (($data = fgetcsv($handle, 1000, ";"))) {
          if($row == 0){
            // si on est à la première ligne du csv, on récupère les clés
            $tabKeys = $data;
          } else {
            if($_GET["numero"] == $data[0]){
              // si on est à la ligne du bon contrat, on stocke les informations
              $ligneSuppr= $row;
              $i = 0;
              foreach ($tabKeys as $key) {
                $contrat[$key] = $data[$i];
                $i ++;
              }
            }
          }
          $row++;
        }
          fclose($handle);
          }

        $target_dir = "../Permis/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $nomFichier = basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


    // Allow certain file formats
    if($imageFileType != "jpg" &&  $imageFileType != "jpeg" && $imageFileType != "png") {
        echo "Désolé, uniquement les format jpg, jpeg, png sont autorisés.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo " Désolé, votre fichier n'a pas pu etre uploader.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            //echo $target_file;
            echo "Votre permis de conduire à bien été ajoutée.";
            rename($target_file, $target_dir.$_SESSION['identifiant'].$contrat["numero"].'.'.$imageFileType);
            //echo $target_dir.$_SESSION['identifiant'].$contrat["numero"].'.'.$imageFileType;

    //ajouter le nom de l'image dans le csv contrats
    $donnee=file("../csv/contrats.csv"); // on met le contenu du fichier dans $donnee

    $infocontrats = fopen("../csv/contrats.csv", "w"); // ouvre le fichier en droit d'écriture
        fputs($infocontrats,''); // on le vide

        $i=0;
        foreach($donnee as $d)
        //
        {
            if($i!=$ligneSuppr){
                fputs($infocontrats,$d);
            }else{
                fputs($infocontrats,'');
            }
            $i=$i+1;
        }

    fclose($infocontrats);

    $fp = fopen('../csv/contrats.csv', 'a+');
    $moncontrats=array(
        array($contrat["numero"],
        $contrat["date"],
        $contrat["codeAssureur"],
        $contrat["identifiantAssure"],
        $contrat["numCarteVerte"],
        $contrat["carteGrise"],
        $_SESSION['identifiant'].$contrat["numero"].'.'.$imageFileType,
        )
    );
    foreach ($moncontrats as $fields) {
        fputcsv($fp, $fields,";");
    }
    fclose($fp);



        } else {
            echo "Désolé, il y a eu une erreur lors de l'upload de votre fichier.";
        }
    }


    ?>

     <p><a href="pageProfil.php">Retour</a></p>
   </div>


</body>
</html>
