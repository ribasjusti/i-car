<?php
    session_start();
    //vérification que l'utilisateur soit connecté pour accéder à la page, et qu'il soit un gestionnaire
  	if (!isset($_SESSION['identifiant'])|| ($_SESSION['profil']!="gestionnaire")){
  			header('Location: ../index.php');
  			exit();
  	}


    /* Fonction pour récupérer les info du gestionnaire */
    function recupInfoGestionnaire(){
      $row = 0;
      if (($handle = fopen("../csv/assureurs.csv", "r"))) {
	    	while (($data = fgetcsv($handle, 1000, ";"))) {
					if($data[0] == $_SESSION['codeAssureur']){
            $_SESSION["nom"] = $data[1];
            $_SESSION["adresse"] = $data[2];
            $_SESSION["ville"] = $data[3];
            $_SESSION["codePostal"] = $data[4];
            $_SESSION["pays"] = $data[5];
            $_SESSION["tel"] = $data[6];
            $_SESSION["degatsAssurés"] = $data[7];
          }
					$row++;
	    	}
				fclose($handle);
	    }
    }

    recupInfoGestionnaire();
?>

<html>
<head>
	<title>I-Car</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" href="../img/icon.png">
  <link rel="stylesheet" type="text/css" href="../css/designGlobal.css" />
  <link rel="stylesheet" type="text/css" href="../css/navbar.css" />
  <link rel="stylesheet" type="text/css" href="../css/pageAccueil.css" />
</head>
<body>
  <div class="nav">
    <input type="checkbox" id="nav-check">
    <div class="nav-header">
      <div class="nav-title">
        <a href="./profilGestionnaire.php"><img style="width: 50px" src="../img/icon.png"/></a>
      </div>
    </div>
    <div class="nav-btn">
      <label for="nav-check">
        <span></span>
        <span></span>
        <span></span>
      </label>
    </div>

    <div class="nav-links">
      <a href="./php/pageAccueilAssures.php">Assurés</a>
      <a href="./php/pageAccueilSinistres.php">Sinistres</a>
      <a href="./php/messagerie.php">Messagerie</a>
      <a href="./php/tickets.php">Tickets</a>
      <a href="../deconnexion.php?connexion=out">Déconnexion</a>
    </div>
  </div>


	<div class="titre">
		<h1>Bienvenue sur le profil Gestionnaire</h1>
	</div>

	<div class="menu_icone">
    <div class="icone"><a href="./php/pageAccueilAssures.php"><img style="height: 150px;" src="../img/assure.png"/><p>Assurés</p></a></div>
    <div class="icone"><a href="./php/pageAccueilSinistres.php"><img style="height: 150px;" src="../img/sinistre.png"/><p>Sinistres</p></a></div>
    <div class="icone"><a href="./php/messagerie.php"><img style="height: 150px;" src="../img/messagerie.png"/><p>Messagerie</p></a></div>
    <div class="icone"><a href="./php/tickets.php"><img style="height: 150px;" src="../img/ticket.png"/><p>Tickets</p></a></div>
  </div>

  <?php

    /*Fonction pour récupérer tous les info du contrat*/
    function recupInfoContrat(){
      $row = 0;
      $contrat = array(); //tableau qui contient les info du contrat
      $tabKeys = array(); //tableau qui contient toutes les clés du tableau $contrat
      if (($handle = fopen("../csv/contrats.csv", "r"))) {
        while (($data = fgetcsv($handle, 1000, ";"))) {
          if($row == 0){
            // si on est à la première ligne du csv, on récupère les clés
            $tabKeys = $data;
          } else {
            if($_SESSION["numeroContrat"] == $data[0]){
              // si on est à la ligne du bon contrat, on stocke les informations
              $i = 0;
              foreach ($tabKeys as $key) {
                $contrat[$key] = $data[$i];
                $i ++;
              }
            }
          }
          $row++;
        }
        fclose($handle);
      }
      return($contrat);
    }


    /*Fonction pour récupérer tous les info de la carte verte*/
    function recupInfoCarteVerte($numeroCarteVerte){
      $row = 0;
      $carteVerte = array(); //tableau qui contient les info de la carte verte
      $tabKeys = array(); //tableau qui contient toutes les clés du tableau $carteVerte
      if (($handle = fopen("../csv/cartesVertes.csv", "r"))) {
        while (($data = fgetcsv($handle, 1000, ";"))) {
          if($row == 0){
            // si on est à la première ligne du csv, on récupère les clés
            $tabKeys = $data;
          } else {
            if($numeroCarteVerte == $data[0]){
              // si on est à la ligne de la bonne carte verte, on stocke les informations
              $i = 0;
              foreach ($tabKeys as $key) {
                $carteVerte[$key] = $data[$i];
                $i ++;
              }
            }
          }
          $row++;
        }
        fclose($handle);
      }
      return($carteVerte);
    }



    /*Fonction pour récupérer tous les info de l'assuré*/
    function recupInfoAssure($identifiant){
      $row = 0;
      $assure = array(); //tableau qui contient les info de l'assuré
      $tabKeys = array(); //tableau qui contient toutes les clés du tableau $assure
      if (($handle = fopen("../csv/assures.csv", "r"))) {
        while (($data = fgetcsv($handle, 1000, ";"))) {
          if($row == 0){
            // si on est à la première ligne du csv, on récupère les clés
            $tabKeys = $data;
          } else {
            if($identifiant == $data[0]){
              // si on est à la ligne de la bonne carte verte, on stocke les informations
              $i = 0;
              foreach ($tabKeys as $key) {
                $assure[$key] = $data[$i];
                $i ++;
              }
            }
          }
          $row++;
        }
        fclose($handle);
      }
      return($assure);
    }


    /* Fonction pour afficher un contrat */
    function afficherContrat($contrat, $carteVerte, $assure){
      $validiteContrat = RenouvellementDate($contrat["date"]);
      $validite = array("A", "B", "BG", "CY", "CZ", "D", "DK", "E", "EST", "F", "FN", "GB", "GR", "H", "I", "IRL", "IS", "L", "LT", "LV", "M", "N", "NL", "P", "PL", "RO", "S", "SK", "SLO", "CH", "AL", "AND", "BIH", "BY", "HR", "IL", "IR", "MA", "MD", "MK", "RUS", "SRB", "TN", "TR", "UA");
 		  $dateDeb = explode("-", $carteVerte["dateDebut"]);
 		  $annee = $dateDeb[0] + 1;
 		  $dateFin = $annee."-".$dateDeb[1]."-".$dateDeb[2];
      echo("<div class='affichage' style='margin-top:100px'>
        <h2>Voici les informations correspondant au QR code scanné</h2>
      </div>
      <div class='contrat'>
        <p>Validité du contrat : ".$validiteContrat."</p>
        <h4>Informations de la carte verte : </h4>
        <p>Numéro de carte verte : ".$carteVerte["numero"]."</p>
        <p>Valable du : ".$carteVerte["dateDebut"]." , au : ".$dateFin."</p>
        <p>Code pays / code assureur - numéro : ".$carteVerte["codePays"]." ".$carteVerte["codeAssureur"]."</p>
        <p>Numéro d'immatriculation : ".$carteVerte["plaque"]."</p>
        <p>Catégorie du véhicule : ".$carteVerte["catégorie"]."</p>
        <p>Marque du véhicule : ".$carteVerte["marque"]."</p>
        <p>Validité territoriale : </p>
        <table>");
      $vInterdites = explode("-", $carteVerte["validiteTerritoriale"]);
      echo("<tr>");
      $i = 1;
      foreach ($validite as $value) {
        if(in_array($value, $vInterdites)){
          echo("<td style='background-color: #5a5958'>".$value."</td>");
        } else {
          echo("<td>".$value."</td>");
        }
        if($i % 14 == 0){
          echo("</tr><tr>");
        }
        $i ++;
      }
      echo("</tr></table>
        <p>Nom du souscripteur : ".$assure["nom"]." ".$assure["prenom"]."</p>
        <p>Adresse du souscripteur : ".$assure["adresse"]." ".$assure["ville"]." ".$assure["codePostal"]."</p>
        <p>Carte délivrée par : ".$_SESSION["nom"]." | ".$_SESSION["adresse"]." ".$_SESSION["ville"]." ".$_SESSION["codePostal"]."</p>");

        //Carte grise
        if ($contrat["carteGrise"] == ''){
          echo "<h4>Carte Grise :</h4>";
          echo "<h4>Pas de carte grise ajoutée.</h4>";

        }else{
          echo "<h4>Carte Grise :</h4>";
          echo "<div id='catreGrise' style='text-align:center;'>";
          echo '<img src="../CartesGrises/'.$contrat["carteGrise"].'" alt="Carte grise" style="width:90%;">';
          echo "</div>";
        }

        //Permis
        if ($contrat["permis"] == ''){
          echo "<h4>Permis de conduire :</h4>";
          echo "<h4>Pas de permis de conduire ajoutée.</h4>";

        }else{
          echo "<h4>Permis :</h4>";
          echo "<div id='permis' style='text-align:center;'>";
          echo '<img src="../Permis/'.$contrat["permis"].'" alt="Permis" style="width:90%;">';
          echo "</div>";
        }

      echo ("<h4>QR code associé à ce contrat : </h4>
         <a href='../QRcode/".$contrat["numero"].".png' download='".$contrat["numero"].".png'><img src='../QRcode/".$contrat["numero"].".png'/></a>
         <p><i>(Cliquez sur le QR code pour le télécharger)</i></p>
        </div>
      </div>");
    }

    function RenouvellementDate($dateContrat){
      $annee = date("Y");
			$mois = date("m");
			$jour = date("d");
			$dateM = explode('-', $dateContrat);
			$validite = "valide";
 			if ($dateM[0] < $annee){
				$validite = "invalide";
				if ($annee - $dateM[0] == 1 && $dateM[1] >= $mois){
					$validite = "valide";
					if ($dateM[2] >= $jour){
						$validite = "valide";
					} else {
						$validite = "invalide";
					}
				}
			}
 			return($validite);
 		}

    //si un QR code a été scanné :
    if(isset($_SESSION["numeroContrat"]) && $_SESSION["numeroContrat"] != ""){
      $contrat = recupInfoContrat();
      $carteVerte = recupInfoCarteVerte($contrat["numCarteVerte"]);
      $assure = recupInfoAssure($contrat["identifiantAssure"]);
      afficherContrat($contrat, $carteVerte, $assure);
    }

  ?>
</body>
